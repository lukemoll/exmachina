/* VirtualMemoryTest.java
 * Equivalent of RegisterTest for VirtualMemory
 */
package exmachina.test;

import exmachina.core.VirtualMemory;

public class VirtualMemoryTest {

	public static void main(String[] args) {
		VirtualMemory mem = new VirtualMemory();
		
		
		testValue(mem,0,1);
		testValue(mem,1,4294967295l);
		testValue(mem,2,3686635541l);//Asymmetric binary pattern to check endian-ness is preserved (1101 1011 1011 1101 1001 1000 0001 0101)
		testValue(mem,3,-1);
		testValue(mem,4,4294967296l);

	}
	
	private static void testValue(VirtualMemory mem, int index, long value) {
		try {
			System.out.println("[" + index + "]:");
			System.out.println(" in:\t" + String.format("%32s", Long.toBinaryString(value)).replace(' ', '0') + "\t" + value);
			mem.put(index, value);//Error thrown here
			System.out.println();
			long outValue = mem.get(index);
			
			System.out.println(" out:\t" + String.format("%32s", Long.toBinaryString(outValue)).replace(' ', '0') + "\t" + outValue);
			System.out.println();
		} catch (Error e) {
			System.out.println("Error caught!");
			System.out.println(e.getMessage());
		}
	}

}
