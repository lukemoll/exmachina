/* InstructionFactoryTest.java
 * Tests all (non-branching) commands with expected values.
 */
package exmachina.test;

import exmachina.core.InstructionFactory;
import exmachina.core.InvalidInstructionException;
import exmachina.core.VirtualMachine;

public class InstructionFactoryTest {

	public static void main(String[] args) {
		VirtualMachine vm = new VirtualMachine();
		
		try {
			System.out.println("Expecting R1 == 16");
			InstructionFactory.DecodeInstruction("MOV R1, #16").accept(vm);
			System.out.println("R1: " + vm.GPRs[1]);
			System.out.println();
			
			System.out.println("Expecting R2 == 24");
			InstructionFactory.DecodeInstruction("ADD R2, R1, #8").accept(vm);
			System.out.println("R2: " + vm.GPRs[2]);
			System.out.println();
			
			System.out.println("Expecting GREATER_THAN");
			InstructionFactory.DecodeInstruction("CMP R2, R1").accept(vm);
			System.out.println("Comparison result: " + vm.getLastComparison());
			System.out.println();
			
			System.out.println("Expecting R3 == 8");
			InstructionFactory.DecodeInstruction("AND R3, R2, #12").accept(vm);
			System.out.println("R3: " + vm.GPRs[3]);
			System.out.println();
			
			System.out.println("Expecting R4 == 10");
			InstructionFactory.DecodeInstruction("ORR R4, R3, #10").accept(vm);
			System.out.println("R4: " + vm.GPRs[4]);
			System.out.println();
			
			System.out.println("Expecting R5 == 18");
			InstructionFactory.DecodeInstruction("EOR R5, R4, R2").accept(vm);
			System.out.println("R5: " + vm.GPRs[5]);
			System.out.println();
			
			System.out.println("Expecting R6 == 4294967277");
			InstructionFactory.DecodeInstruction("MVN R6, R5").accept(vm);
			System.out.println("R6: " + vm.GPRs[6]);
			System.out.println();
			
			System.out.println("Expecting R7 == 65535");
			InstructionFactory.DecodeInstruction("LSR R7, R6, #16").accept(vm);
			System.out.println("R7: " + vm.GPRs[7]);
			System.out.println();
			
			System.out.println("Expecting R8 == 131070");
			InstructionFactory.DecodeInstruction("LSL R8, R7, #1").accept(vm);
			System.out.println("R8: " + vm.GPRs[8]);
			System.out.println();
			
			System.out.println("Expecting mem location 10 == 131070");
			InstructionFactory.DecodeInstruction("STR R8, 10").accept(vm);
			System.out.println("10: " + vm.MainMemory.get(10));
			System.out.println();
			
			System.out.println("Expecting R9 == 131070");
			InstructionFactory.DecodeInstruction("LDR R9, 10").accept(vm);
			System.out.println("R9: " + vm.GPRs[9]);
			System.out.println();
			
			System.out.println("fin");
			
			
		} catch (InvalidInstructionException e) {
			e.printStackTrace();
		}

	}

}
