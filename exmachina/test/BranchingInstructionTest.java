/* BranchingInstructionTest.java
 * Used to test the (then-new) B and B<code> commands
 */
package exmachina.test;

import java.util.Arrays;
import java.util.function.Consumer;

import exmachina.core.InstructionFactory;
import exmachina.core.InvalidInstructionException;
import exmachina.core.VirtualMachine;

public class BranchingInstructionTest {

	public static void main(String[] args) {
		String[] instructions = new String[] {
				"MOV R0, #10",
				"top:",
				"SUB R0, R0, #1",
				"CMP R0, #0",
				"BNE top",
				"HALT"
		};
		
		System.out.println("[BIT] " + instructions.length);
		
		Consumer<VirtualMachine>[] program = Arrays.stream(instructions).map((String i) -> {try {
			return InstructionFactory.DecodeInstruction(i);
		} catch (InvalidInstructionException e) {
			return InstructionFactory.NOP;
		}}).peek(System.out::println).toArray((size) -> (Consumer<VirtualMachine>[]) new Consumer[size]);//we know what we're doing;

		
		VirtualMachine vm = new VirtualMachine(program);
		do {
			System.out.println("R0: " + vm.GPRs[0]);
		} 
		while(vm.executeNext());
	}

}
