/* SyntaxHighlightingTest.java
 * 
 */
package exmachina.test.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.regex.Pattern;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

import exmachina.ui.DocumentSyntaxFormatter;

public class SyntaxHighlightingTest {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
			JTextPane editor = new JTextPane();
				editor.setText("MOV R0, #10\n// this is a comment\n#this is also a commment\n@ and this too!");
//				StyledDocument doc = editor.getStyledDocument();
//				
					SimpleAttributeSet commentStyle = new SimpleAttributeSet();
						StyleConstants.setForeground(commentStyle, Color.LIGHT_GRAY);
						StyleConstants.setItalic(commentStyle, true);
					
					SimpleAttributeSet commandNameStyle = new SimpleAttributeSet();
						StyleConstants.setBold(commandNameStyle, true);
						StyleConstants.setForeground(commandNameStyle, Color.BLUE);
					SimpleAttributeSet commandParamsStyle = new SimpleAttributeSet();
						StyleConstants.setBold(commandParamsStyle, true);
						StyleConstants.setForeground(commandParamsStyle, Color.GREEN);
					
					SimpleAttributeSet branchCommandStyle = new SimpleAttributeSet();
						StyleConstants.setBold(branchCommandStyle, true);
						StyleConstants.setForeground(branchCommandStyle, Color.ORANGE);
					SimpleAttributeSet branchNameStyle = new SimpleAttributeSet();
						StyleConstants.setItalic(branchNameStyle, true);
						StyleConstants.setForeground(branchNameStyle, Color.ORANGE);
					
					
					
				DocumentSyntaxFormatter dsf = new DocumentSyntaxFormatter(editor.getStyledDocument());
				dsf.associate(Pattern.compile("^(?:[#@]|\\/{2}).*$",Pattern.MULTILINE), commentStyle);
				dsf.associateAll(Pattern.compile("^([A-Z]{1,3}) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, commandNameStyle, commandParamsStyle);
				dsf.associateAll(Pattern.compile("^(B(?:[A-Z]{2})?) ([A-Z]+)$", Pattern.MULTILINE), null, branchCommandStyle, branchNameStyle);
				dsf.associateAll(Pattern.compile("^([A-Z]+)(:)$", Pattern.MULTILINE), null, branchNameStyle, branchCommandStyle);
				
			editor.setPreferredSize(new Dimension(512,512));
		frame.add(editor);
		frame.pack();
		frame.setTitle("SyntaxHighlightingTest - ExMachina");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
