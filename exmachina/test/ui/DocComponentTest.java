/* DocComponentTest.java
 * Used to test the layout and behaviour of the (then-new) DocComponent
 */
package exmachina.test.ui;

import javax.swing.JFrame;

import exmachina.ui.DocComponent;

public class DocComponentTest {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.add(new DocComponent("MOV", "er, st, idk","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas scelerisque aliquet iaculis. Ut maximus, sapien et porttitor efficitur, turpis nisi ullamcorper dolor, vitae tempor odio justo non ligula. Mauris lacinia tellus arcu, eu mollis lorem congue nec. Vestibulum et lorem hendrerit, ornare nunc non, posuere odio. Etiam non neque finibus, bibendum odio at, bibendum velit. Morbi lacinia tincidunt sapien, eu volutpat augue vestibulum vel. "));
		frame.pack();
		frame.setSize(300, 400);
		frame.setResizable(true);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
