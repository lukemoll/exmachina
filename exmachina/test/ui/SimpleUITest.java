/* SimpleUITest.java
 * non-functional demo to test layout and basic styling, basis of RootFrame 
 */
package exmachina.test.ui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.*;

import exmachina.ui.DocComponent;
import exmachina.ui.DocItem;
import exmachina.ui.DocUtils;

public class SimpleUITest extends JFrame {

	public static void main(String[] args) {
		new SimpleUITest();

	}
	
	public SimpleUITest() {
		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.LINE_AXIS));
		
		JPanel editorPanel = new JPanel();
			editorPanel.setLayout(new BoxLayout(editorPanel, BoxLayout.PAGE_AXIS));
			JTextArea mainEditor = new JTextArea();
			editorPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			editorPanel.add(mainEditor);
			editorPanel.setPreferredSize(new Dimension(512,512));
		rootPanel.add(editorPanel);
		
		JPanel sidePanel = new JPanel();
			sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.PAGE_AXIS));
			sidePanel.add(Box.createHorizontalStrut(300));
			
			JPanel docsPanel = new JPanel();
				docsPanel.setBackground(Color.WHITE);
				docsPanel.setLayout(new BoxLayout(docsPanel, BoxLayout.PAGE_AXIS));
					for(DocItem di : DocUtils.COMMANDS) {
						docsPanel.add(new DocComponent(di));
					}
			JScrollPane docsScroller = new JScrollPane(docsPanel);
			docsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			docsScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			docsScroller.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			docsScroller.setPreferredSize(new Dimension(300,512));
			sidePanel.add(docsScroller);	
			
			JPanel buttonPanel = new JPanel();
				buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
				buttonPanel.add(new JLabel("ButtonPanel!"));
				buttonPanel.setPreferredSize(new Dimension(300,128));
			sidePanel.add(buttonPanel);
		rootPanel.add(sidePanel);
		
		this.add(rootPanel);
		this.pack();
		this.setTitle("SimpleUITest - ExMachina");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}

}
