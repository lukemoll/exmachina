/* RegisterTest.java
 * Tests all types of Register data (correct, boundary, extreme, incorrect)
 */
package exmachina.test;

import exmachina.core.VirtualMachine;

public class RegisterTest {

	public static void main(String[] args) {
		VirtualMachine.Register reg = new VirtualMachine.Register();
		
		System.out.println("Testing initial value");
		System.out.println(" Register value should == 0");
		System.out.println(reg.getValue());
		System.out.println("Success: " + (reg.getValue() == 0));
		System.out.println();

		System.out.println("Testing 2^32-1");
		System.out.println(" Max value should be able to be held in 32 bits, expecting 4294967295");
		reg.setValue(4294967295l);
		System.out.println(reg.getValue());
		System.out.println("Success: " + (reg.getValue() == 4294967295l));
		System.out.println();
		
		boolean errorThrown = false;
		System.out.println("Testing negative value (-1)");
		System.out.println(" Register should not allow two's complement, so expecting error thrown");
		try {
			reg.setValue(-1);
		} catch (Error e) {
			e.printStackTrace();
			System.out.println("Error caught!");
			errorThrown = true;
		}
		System.out.println(reg.getValue());
		System.out.println("Success: " + errorThrown);
		System.out.println();
		

		errorThrown = false;
		System.out.println("Testing 2^32");
		System.out.println(" Register should not allow integer overflow, so expecting error thrown");
		try {
			reg.setValue(4294967296l);
		}
		catch (Error e) {
			e.printStackTrace();
			System.out.println("Error caught!");
			errorThrown = true;
		}
		System.out.println("Success: " + errorThrown);
		System.out.println();
		
	}

}
