/* REPL.java
 * Read-Evaluate-Print-Loop. Executes non-branching instructions against a VirtualMachine.
 */
package exmachina.test;

import java.util.Scanner;
import java.util.function.Consumer;

import exmachina.core.InstructionFactory;
import exmachina.core.InvalidInstructionException;
import exmachina.core.VirtualMachine;

public class REPL {

	public static void main(String[] args) {
		VirtualMachine vm = new VirtualMachine();
		
		Scanner in = new Scanner(System.in);
		String input;
		Consumer<VirtualMachine> instruction;
		while(vm.isRunning()) {
			input = in.nextLine();
			try {
				instruction = InstructionFactory.DecodeInstruction(input.toUpperCase());
				if(instruction != null) {
					instruction.accept(vm);
					printRegisters(vm);
					System.out.println();
				}
				else {
					System.err.println("Valid instruction not found.");
				}
				
			} catch (InvalidInstructionException e) {
				System.err.println("Invalid instruction:");
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		}
		in.close();

	}
	
	private static void printRegisters(VirtualMachine vm) {
		for(int i=0;i<4;i++) {
			System.out.println("R" + i*3 + ": " + vm.GPRs[i*3] + "\tR" + (1 + i*3) + ": " + vm.GPRs[1 + i*3] + "\tR" + (2+i*3) + ": " + vm.GPRs[2 + i*3]);
		}
		System.out.println("R12: " + vm.GPRs[12]);
	}

}
