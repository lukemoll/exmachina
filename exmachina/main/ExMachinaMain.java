/* ExMachinaMain.java
 * Main class, entry point for program.
 * Simply invokes constructor of RootFrame, all the magic happens in there :)
 */
package exmachina.main;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import exmachina.ui.RootFrame;

public class ExMachinaMain {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//Sets the look&feel to the OS default
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			//Oh well. It won't look fancy
		}
		new RootFrame();

	}

}
