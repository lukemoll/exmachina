/* VirtualMemory.java
 * Represents 65535 4-byte memory locations with appropriate input validation and error handling.  
 */
package exmachina.core;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class VirtualMemory {

	public final int length = 65535;
	
	private byte[][] memory = new byte[length][4];
	
	public long get(int index) {
		if(index >= 0 && index <= length) {
			int storedValue = ByteBuffer.wrap(memory[index]).getInt();
			long returnValue = storedValue & 4294967295l;
			return returnValue;
		}
		else return -1;
		
	}
	
	public void put(int index, long value) {
		if(index >= 0 && index <= length && value >= 0 && value < 4294967296l) {//value must be contained in four bytes
			ByteBuffer buf = ByteBuffer.allocate(4);
			int storedValue;
			storedValue = (int) (value & 4294967295l); //cannot have type conversion error
			storedValue = (int) ((value & 2147483648l)==0?storedValue:(storedValue | 2147483648l));//Store as signed int
			buf.putInt(storedValue);
			memory[index] = buf.array();
		}
		else {
			throw new Error("Cannot hold value in 32 bits. No changes made to memory location.");
		}
		System.err.flush();
	}
}
