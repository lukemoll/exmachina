/* VirtualMachine.java
 * Represents the registers of the CPU.
 */
package exmachina.core;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class VirtualMachine {

	
	
	public static class Register {
		/* Represents a General-purpose Register within the processor
		 * Necessary as 32-bit unsigned integers are not available in Java		 * 
		 */
		
		public static final long MAX_VALUE = 4294967295l;
		private int value;
		
		public int _getInner() {return value;}
		
		public Register() {
			this.setValue(0);
		}
		
		public void setValue(int newValue) {
			setValue((long)newValue);
		}
		
		public void add(int toAdd) {
			value += toAdd;
		}
		
		public void setValue(Long newValue) {
			//Check for valid bounds
			if(newValue < 4294967296l && newValue >= 0)
				value = Integer.parseUnsignedInt(newValue.toString());
				else {
					throw new Error("Value " + newValue + " + cannot be held in 32 unsigned bits");//should be handled when changing value
				}
		}
		
		public long getValue() {
			return Integer.toUnsignedLong(value);
		}
		
		public String toString() {
			return Integer.toUnsignedString(value);
		}
	}

	public enum ComparisonResult {EQUAL, GREATER_THAN, LESS_THAN}
	
	public final Register[] GPRs = new Register[13];
	ComparisonResult lastComparison = ComparisonResult.EQUAL;
	
	public final VirtualMemory MainMemory = new VirtualMemory();
	private Consumer<VirtualMachine>[] program;
	
	private Map<String, Integer> labels = new HashMap<String, Integer>();
	private final boolean MODE_DEMO;//if TRUE, does not execute commands sequentially, and is only used for single, non-branching commands.
	public int pointer = 0;
	private boolean running = true;
	
	
	public VirtualMachine(Consumer<VirtualMachine>[] arg0) {
		this.MODE_DEMO = false;
		this.program = arg0;
		for(int i=0; i<GPRs.length; i++) {
			GPRs[i] = new Register();
		}
//		System.out.println("[VM] " + program.length);
	}
	
	public VirtualMachine() {
		this.MODE_DEMO = true;
		for(int i=0; i<GPRs.length; i++) {
			GPRs[i] = new Register();
		}
	}

	
	/**
	 * 
	 * @return True if there is another instruction to execute, false if there is not. Calling this after it returns false is safe, but performs no operation.
	 */
	public boolean executeNext() {
		if(running == false || MODE_DEMO) {return false;}
		//Pointer is zero-based, .length is 1-based. When equal, pointer -> out of bounds
		if(pointer >= program.length) {
			this.halt();
			return false;
		}
		Consumer<VirtualMachine> instruction;
		do {
			instruction = program[pointer];
		}
		while(instruction == InstructionFactory.NOOP);
		instruction.accept(this);
		pointer++;
		
		return true;
	}
	
	public void halt() {this.running = false;}
	
	public ComparisonResult compare(long lhs, long rhs) {
		if(lhs == rhs) {return ComparisonResult.EQUAL;}
		else if(lhs > rhs) {return ComparisonResult.GREATER_THAN;}
		else if(lhs < rhs) {return ComparisonResult.LESS_THAN;}
		return null;//never reachable
	}
	
	public void addLabel(String label) {
		this.addLabel(label, this.pointer);
	}
	
	//
	public void addLabel(String label, int pointer) {
		if(!labels.containsKey(label.toUpperCase())) {
			labels.put(label.toUpperCase(), pointer);
		}
		else {
			//Should-be unreachable situation, logging for debug purposes
			if(this.pointer != this.getPointer(label)) {
				System.out.println("Existing label:\t" + label + "[" + this.getPointer(label) + "]");
				System.out.println("New label:\t" + label + "[" + pointer + "]"); 
			}
		}
	}
	
	public int getPointer(String label) {
		return labels.getOrDefault(label.toUpperCase(), this.pointer);
	}

	public boolean isRunning() {
		return running;
	}
	
	//Used getter-only design pattern to prevent unsanitised comparisons
	public ComparisonResult getLastComparison() {return this.lastComparison;}
	
	
	
}
