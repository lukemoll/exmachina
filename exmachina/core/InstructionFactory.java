/* InstructionFactory.java
 * Static utility class to convert a single line instruction into a Consumer<VirtualMachine>, which
 * will modify the state of the VirtualMachine in accordance with the string passed to 
 * InstructionFactory.   
 */
package exmachina.core;

import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstructionFactory {

	private InstructionFactory() {}// Singleton constructor - doesn't allow instances to be created
	
	public static final Consumer<VirtualMachine> NOP = (vm) -> {}, NOOP = NOP;//Use NOOP instead of null Consumers to fail-safe
	
	//Regular Expressions for all commands in AAL Spec PDF
	private static final Pattern INSTRUCTION_LDR = Pattern.compile("^LDR R(\\d+), (\\d+)$");
	private static final Pattern INSTRUCTION_STR = Pattern.compile("^STR R(\\d+), (\\d+)$");
	private static final Pattern INSTRUCTION_ADD = Pattern.compile("^ADD R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_SUB = Pattern.compile("^SUB R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_MOV = Pattern.compile("^MOV R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_CMP = Pattern.compile("^CMP R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_B = Pattern.compile("^B (\\w+)$");
	private static final Pattern INSTRUCTION_B_COND = Pattern.compile("^B(\\w\\w) (\\w+)$");
	private static final Pattern INSTRUCTION_LABEL = Pattern.compile("^([a-zA-Z]+):$");
	private static final Pattern INSTRUCTION_AND = Pattern.compile("^AND R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_ORR = Pattern.compile("^ORR R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_EOR = Pattern.compile("^EOR R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_MVN = Pattern.compile("^MVN R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_LSL = Pattern.compile("^LSL R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_LSR = Pattern.compile("^LSR R(\\d+), R(\\d+), (#\\d+|R\\d+)$");
	private static final Pattern INSTRUCTION_HALT = Pattern.compile("^HALT$");
	
	private static final Pattern COMMENT = Pattern.compile("^(?:[#@]|\\/{2}).*$");
	private static final Pattern WHITESPACE = Pattern.compile("^\\s*$");
	
	public static Consumer<VirtualMachine> DecodeInstruction(String instruction) throws InvalidInstructionException {
		Matcher match = INSTRUCTION_LDR.matcher(instruction);
		if(match.matches()) {
			int ref, destination;
			destination = Integer.parseInt(match.group(1));
			ref = Integer.parseInt(match.group(2));
			return vm -> {vm.GPRs[destination].setValue(vm.MainMemory.get(ref));};	
		}
		/* complex else if explained:
		 * an assignment ( x = 2 ) returns the left-hand side of the assignment
		 * so ((x = 2) == 2) == TRUE
		 * 
		 * therefore (match = INSTRUCTION_*.matcher("...")) returns a Matcher 
		 * on which the .matches() method can be called
		 * which returns a boolean
		 * allowing this neat one-liner to be used ^-^
		 */
		else if((match = INSTRUCTION_STR.matcher(instruction)).matches()) {
			int ref, register;
			register = Integer.parseInt(match.group(1));
			ref = Integer.parseInt(match.group(2));
			return vm -> {vm.MainMemory.put(ref, (int) vm.GPRs[register].getValue());};
		}
		else if((match = INSTRUCTION_ADD.matcher(instruction)).matches()) {
			String op2 = match.group(3);
			int destination = Integer.parseInt(match.group(1));
			int register = Integer.parseInt(match.group(2));
			if(op2.startsWith("#")) {//#5 denotes denary literal value 5, substring(1) removes the first character '#', leaving only the value to be parsed.
				return vm -> {vm.GPRs[destination].setValue(vm.GPRs[register].getValue() + Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {//R5 denotes value of register 5
				return vm -> {vm.GPRs[destination].setValue(vm.GPRs[register].getValue() + vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_SUB.matcher(instruction)).matches()) {
			String op2 = match.group(3);
			int destination = Integer.parseInt(match.group(1));
			int register = Integer.parseInt(match.group(2));
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[destination].setValue(vm.GPRs[register].getValue() - Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[destination].setValue(vm.GPRs[register].getValue() - vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_MOV.matcher(instruction)).matches()) {
			String op2 = match.group(2);
			int destination = Integer.parseInt(match.group(1));
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[destination].setValue(Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[destination].setValue(vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
			
		}
		else if((match = INSTRUCTION_CMP.matcher(instruction)).matches()) {
			int register = Integer.parseInt(match.group(1));
			long rhs;
			String op2 = match.group(2);
			if(op2.startsWith("#")) {
				rhs = Long.parseLong(op2.substring(1));
				return vm -> {vm.lastComparison = vm.compare((int)vm.GPRs[register].getValue(), rhs);};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.lastComparison = vm.compare((int)vm.GPRs[register].getValue(), (int)vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_B.matcher(instruction)).matches()) {
			String label = match.group(1);
			return vm -> {vm.pointer = vm.getPointer(label);};
		}
		else if((match = INSTRUCTION_B_COND.matcher(instruction)).matches()) {
			String label = match.group(2);
			switch(match.group(1).toUpperCase()) {
				case "EQ":
					return vm -> {if(vm.lastComparison == VirtualMachine.ComparisonResult.EQUAL){vm.pointer = vm.getPointer(label);}};
				case "NE":
					return vm -> {if(vm.lastComparison != VirtualMachine.ComparisonResult.EQUAL){vm.pointer = vm.getPointer(label);}};
				case "GT":
					return vm -> {if(vm.lastComparison == VirtualMachine.ComparisonResult.GREATER_THAN){vm.pointer = vm.getPointer(label);}};
				case "LT":
					return vm -> {if(vm.lastComparison == VirtualMachine.ComparisonResult.LESS_THAN){vm.pointer = vm.getPointer(label);}};
				default:
					throw new InvalidInstructionException("Invalid condition code: " + match.group(1).toUpperCase());
			}
		}
		else if((match = INSTRUCTION_LABEL.matcher(instruction)).matches()) {
			String label = match.group(1);
			return vm -> {vm.addLabel(label);};
		}
		else if((match = INSTRUCTION_AND.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			int op1 = Integer.parseInt(match.group(2));
			String op2 = match.group(3);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() & Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() & vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_ORR.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			int op1 = Integer.parseInt(match.group(2));
			String op2 = match.group(3);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() | Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() | vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_EOR.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			int op1 = Integer.parseInt(match.group(2));
			String op2 = match.group(3);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() ^ Long.parseLong(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() ^ vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_MVN.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			String op2 = match.group(2);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue((~Long.parseLong(op2.substring(1))&VirtualMachine.Register.MAX_VALUE));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(~(vm.GPRs[Integer.parseInt(op2.substring(1))].getValue())&VirtualMachine.Register.MAX_VALUE);};
			}
		}
		else if((match = INSTRUCTION_LSL.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			int op1 = Integer.parseInt(match.group(2));
			String op2 = match.group(3);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() << Integer.parseInt(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() << vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_LSR.matcher(instruction)).matches()) {
			int result = Integer.parseInt(match.group(1));
			int op1 = Integer.parseInt(match.group(2));
			String op2 = match.group(3);
			if(op2.startsWith("#")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() >> Integer.parseInt(op2.substring(1)));};
			}
			else if(op2.startsWith("R")) {
				return vm -> {vm.GPRs[result].setValue(vm.GPRs[op1].getValue() >> vm.GPRs[Integer.parseInt(op2.substring(1))].getValue());};
			}
			else throw new InvalidInstructionException("Invalid operand2: " + op2);
		}
		else if((match = INSTRUCTION_HALT.matcher(instruction)).matches()) {
			return vm -> {vm.halt();};
		}
		// Since unmatched instructions now throw errors, we must detect non-executing code
		else if((match = COMMENT.matcher(instruction)).matches() || (match = WHITESPACE.matcher(instruction)).matches() || instruction.length() == 0) {
			return NOOP;
		}		
		//If no other patterns match, throw an error. This isn't a valid instruction.
		//Don't need to write out the line in question as the handler does that anyway
		throw new InvalidInstructionException("Unrecognised command.");
	}
	
}