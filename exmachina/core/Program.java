package exmachina.core;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import exmachina.core.Program.CompilationError;

public class Program {

	public static class CompilationError extends Exception {
		
		public CompilationError(String message) {
			super(message);
		}
		
	}

	public static class State {
		public final long[] GPRs;
		public final VirtualMemory memory;
		public final int pointer;
		public final VirtualMachine.ComparisonResult lastComparison;
		public final boolean running;
		public final String instruction;
		
		public static State ofVirtualMachine(VirtualMachine vm, String[] parallelSource) {
			long[] GPRs = new long[13];
			for(int i=0; i<GPRs.length; i++) {
				GPRs[i] = vm.GPRs[i].getValue();
			}
			
			String nextInstruction = vm.pointer >= parallelSource.length?"":parallelSource[vm.pointer];
			//If program is at the end, don't try to access the next instruction.
			
			return new State(GPRs, vm.MainMemory, vm.pointer, vm.lastComparison, vm.isRunning(), nextInstruction);
		}
		
		public State(long[] gprs, VirtualMemory memory, int pointer, VirtualMachine.ComparisonResult lastcomparison, boolean running, String instruction) {
			this.GPRs = gprs;
			this.memory = memory;
			this.pointer = pointer;
			this.lastComparison = lastcomparison;
			this.running = running;
			this.instruction = instruction;
		}
		
		public State(long[] gprs, VirtualMemory memory, int pointer, VirtualMachine.ComparisonResult lastcomparison, boolean running) {
			this(gprs, memory, pointer, lastcomparison, running, "NO SOURCE ATTACHED");
		}
		
		public String toString() {
			StringBuilder buf = new StringBuilder();
			for(int i=0; i < 12; i ++) {
				
			}
			
			
			return buf.toString();
		}
	}

	public final Consumer<VirtualMachine>[] program;
	public final String[] source;
	public final Map<String,Integer> labels;
	
	private final VirtualMachine vm;
	
	private static final Pattern INSTRUCTION_LABEL = Pattern.compile("^([a-zA-Z]+):$");
	
	private Program(Consumer<VirtualMachine>[] program, String[] source, Map<String,Integer> labels) {
		this.program = program;
		this.source = source;
		this.labels = Collections.unmodifiableMap(labels);
		this.vm = new VirtualMachine(program);
		for(Entry<String, Integer> label : this.labels.entrySet()) {
			this.vm.addLabel(label.getKey(), label.getValue());
		}
	}
	
	@SuppressWarnings("unchecked")
	public static Program createFromSource(String[] sourceLines) throws CompilationError {
		Map<String,Integer> labels = new HashMap<String,Integer>();
		ArrayList<Consumer<VirtualMachine>> program = new ArrayList<Consumer<VirtualMachine>>();
		ArrayList<String> source = new ArrayList<String>();
		Matcher labelMatcher;
		
		//First strip comments
		for(int i=0; i<sourceLines.length; i++) {
			Consumer<VirtualMachine> instruction;
			try {
				instruction = InstructionFactory.DecodeInstruction(sourceLines[i]);
				if(instruction != InstructionFactory.NOOP) {
					program.add(instruction);
					source.add(sourceLines[i]);
				}
			} catch (InvalidInstructionException e) {
				//error on line i
				throw new CompilationError("Error on line " + (i + 1) + ": " + e.getMessage() + "\n" + sourceLines[i]);
			}
			
		}
		
		//THEN look for labels - otherwise mismatch will occur
		for(int i=0; i<source.size(); i++) {
			if((labelMatcher = INSTRUCTION_LABEL.matcher(source.get(i))).matches()) {
				labels.put(labelMatcher.group(1), i);
			}
		}
		
//		System.out.println("Program: " + program.size() + "\nSource: " + source.size());
		return new Program(	program.toArray((Consumer<VirtualMachine>[]) new Consumer[program.size()]), 
							source.toArray(new String[source.size()]),
							labels);
	}
	
	public State executeNext() {
		vm.executeNext();
		return State.ofVirtualMachine(vm, this.source);
	}
	
	public State executeNext(Consumer<State> c) {
		vm.executeNext();
		c.accept(State.ofVirtualMachine(vm, this.source));
		return State.ofVirtualMachine(vm, this.source);
	}
	
}
