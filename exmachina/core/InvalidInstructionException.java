/* InvalidInstructionException.java
 * Originally part of InstructionFactory.java, but Java only allows one public class per .java file
 * so moved to its own file. 
 */
package exmachina.core;

public class InvalidInstructionException extends Exception {
	
	
	public InvalidInstructionException(String message) {
		super(message);
	}
	
}