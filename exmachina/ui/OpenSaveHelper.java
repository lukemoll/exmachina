/* OpenSaveHelper.java
 * Contains logic to open and save files, and to handle errors correctly.
 * Methods are called (almost directly) from button handlers attached to RootFrame
 */
package exmachina.ui;

import java.awt.FileDialog;
import java.awt.Frame;
import java.io.*;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class OpenSaveHelper {

	private static class DotAALOnly extends javax.swing.filechooser.FileFilter {

		@Override
		public boolean accept(File f) {
			return f.getName().toLowerCase().endsWith(".aal") | f.isDirectory();
			//Remember, kids, directories are good!
		}

		@Override
		public String getDescription() {
			return "AQA Assembly files (*.aal)";
		}
		
	}
	
	public static boolean saveFile(String fullText, Frame parentWindow) {
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new DotAALOnly());
		if(fc.showOpenDialog(parentWindow) == JFileChooser.APPROVE_OPTION) {//Blocks until user selects a file
			File saveFile = fc.getSelectedFile();
			if (saveFile == null)
				return false;
			//else...
			
			try {
				System.out.println(saveFile.getCanonicalPath());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				/* FileWriter: writes to a File
				 * BufferedWriter: makes more efficient writing since writing to a file is 'costly'
				 * PrintWriter: takes care of automatic flushing ("true") 
				 */
				saveFile.createNewFile();
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(saveFile)), true);
				
				for(String line : fullText.split("\n")) {
					out.println(line);
				}
				
				out.flush();//to be safe
				out.close();
				return true;//written successfully
				
			} catch (IOException e) {
				return false;//an error occurred.
			}
		}
		else {
			return false;
		}
		
		
		
	}
	
	public static String loadFile(Frame parentWindow) throws FileNotFoundException {
		JFileChooser fc = new JFileChooser();
		fc.setFileFilter(new DotAALOnly());
		if(fc.showOpenDialog(parentWindow) == JFileChooser.APPROVE_OPTION){
			File loadFile = fc.getSelectedFile();
			if(loadFile == null)
				return null;
			
			//Else...
			try {
				Scanner in = new Scanner(new BufferedReader(new FileReader(loadFile)));
				StringBuilder text = new StringBuilder();//StringBuilder is more efficient than looped ` string += "..." `
				
				while(in.hasNext()) {
					text.append(in.nextLine() + "\n");
				}
				
				return text.toString();
			} catch (FileNotFoundException e) {
				throw e;//Java can't do multiple returns. Since we're returning a String, we throw the exception to indicate error.
			}
			
		}
		else {
			return null;
		}
	}
	
	
	
}
