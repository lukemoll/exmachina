/* DocUtils.java
 * Essentially the contents of the publicly-available "Assembly language instruction set" document
 * Used as a proof-of-concept for this project which is not released to the public.
 */
package exmachina.ui;

public class DocUtils {

	public static final DocItem[] COMMANDS = new DocItem[] {
			//Since JLabels support HTML tags, < and > must be replaced with the HTML entities < and > in the description
			new DocItem("LDR", "Rd, <memory ref>", "Load the value stored in the memory location specified by &lt;memory ref&gt; into register d."),
			new DocItem("STR", "Rd, <memory ref>", "Store the value that is in register d into the memory location specified by &lt;memory ref&gt;."),
			new DocItem("ADD", "Rd, Rn, <operand2>", "Add the value specified in &lt;operand2&gt; to the value in register n and store the result in register d."),
			new DocItem("SUB", "Rd, Rn, <operand2>", "Subtract the value specified by &lt;operand2&gt; from the value in register n and store the result in register d."),
			new DocItem("MOV", "Rd, <operand2>", "Copy the value specified by &lt;operand2&gt; into register d."),
			new DocItem("CMP", "Rn, <operand2>", "Compare the value stored in register n with the value specified by &lt;operand2&gt;."),
			new DocItem("B", "<label>", "Always branch to the instruction at position &lt;label&gt; in the program."),
			new DocItem("B<condition>", "<label>", "Branch to the instruction at position &lt;label&gt; if the last comparison met the criterion specified by &lt;condition&gt;. Possible values for &lt;condition&gt; and their meanings are:<br>EQ: equal to<br>NE: not equal to<br>GT: greater than<br>LT: less than"),
			new DocItem("AND", "Rd, Rn, <operand2>", "Perform a bitwise logical AND operation between the value in register n and the value specified by &lt;operand2&gt; and store the result in register d."),
			new DocItem("ORR", "Rd, Rn, <operand2>", "Perform a bitwise logical OR operation between the value in register n and the value specified by &lt;operand2&gt; and store the result in register d."),
			new DocItem("EOR", "Rd, Rn, <operand2>", "Perform a bitwise logical XOR (exclusive or) operation between the value in register n and the value specified by &lt;operand2&gt; and store the result in register d."),
			new DocItem("MVN", "Rd, <operand2>", "Perform a bitwise logical NOT operation on the value specified by &lt;operand2&gt; and store the result in register d."),
			new DocItem("LSL", "Rd, Rn, <operand2>", "Logically shift left the value stored in register n by the number of bits specified by &lt;operand2&gt; and store the result in register d."),
			new DocItem("LSR", "Rd, Rn, <operand2>", "Logically shift right the value stored in register n by the number of bits specified by &lt;operand2> and store the result in register d."),
			new DocItem("HALT", "","Stops the execution of the program.")
		};
}
