/* ExecuteFrame.java
 * Window showing the status of the VirtualMachine as instructions are passed to it. 
 */
package exmachina.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.Consumer;

import javax.swing.*;

import exmachina.core.*;
import exmachina.core.Program.CompilationError;
import exmachina.core.Program.State;
import exmachina.core.VirtualMachine.ComparisonResult;
import exmachina.core.VirtualMachine.Register;

public class ExecuteFrame extends JFrame implements ActionListener {

	public VirtualMachine vm;
	private Scanner stdIn = new Scanner(System.in);
	private Program p;
	
	private JLabel[] GPRLabels = new JLabel[13];
	private JLabel lastComparison, nextInstruction;
	private Map<Integer, JLabel[]> memLocations = new HashMap<Integer,JLabel[]>();	
	private JPanel memoryPanel;
	
	private JButton step, runAll;
	
	private boolean running = false;
	
	public ExecuteFrame(String text) throws CompilationError {
		for(int i=0; i<GPRLabels.length; i++) {
			GPRLabels[i] = new JLabel();
			setGPRValue(i,0);
		}
		
		String[] source = text.split("\n");
		
		this.p = Program.createFromSource(source);
		initUI();
		
		this.setVisible(true);
		this.pack();
		this.setTitle("Execution");
	}
	
	private void setGPRValue(int index, long value) {
		GPRLabels[index].setText("R" + index + ": " + value);
	}
	
	private void setLastComparisonValue(ComparisonResult r) {
		StringBuilder text = new StringBuilder("<html>Last comparison: <i>LHS ");
		switch(r) {
			case GREATER_THAN:
				text.append("<b>greater than</b>");
				break;
			case LESS_THAN:
				text.append("<b>less than</b>");
			default://VirtualMachine takes default as EQUAL, so we will too.
			case EQUAL: 
				text.append("<b>equal to</b>");
				break;
		}
		text.append(" RHS</i></html>");
		lastComparison.setText(text.toString());
	}
	
	private void setNextInstruction(String instruction) {
		nextInstruction.setText("Next instruction: " + instruction);
	}
	
	private void setMemoryValue(Integer i, long val) {
		if(!memLocations.containsKey(i)) {
			//Column are laid out like so:
			//Location (dec) | Location (hex) | value (dec)
			memLocations.put(i, new JLabel[] {new JLabel(i + ""), new JLabel(Integer.toHexString(i)), new JLabel(val + "") });
			JLabel[] row = memLocations.get(i);
			
			memoryPanel.add(row[0]);
			memoryPanel.add(row[1]);
			memoryPanel.add(row[2]);
		}
		else {
			JLabel[] row = memLocations.get(i);
			row[2].setText(val + "");
		}
	}
	
	private void initUI() {
		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.PAGE_AXIS));
		
		JPanel GPRPanel = new JPanel(new GridLayout(0,3));
		
			int[] order = new int[] {0,5,9,1,6,10,2,7,11,3,8,12,4};
			Arrays.stream(order).forEachOrdered(i -> GPRPanel.add(GPRLabels[i]));
		rootPanel.add(GPRPanel);
			
		rootPanel.add(new JSeparator());
		
		JPanel comparisonPanel = new JPanel(new FlowLayout());
			lastComparison = new JLabel();	
			setLastComparisonValue(ComparisonResult.EQUAL);
			comparisonPanel.add(lastComparison);
		rootPanel.add(comparisonPanel);
		
		
		memoryPanel = new JPanel(new GridLayout(0,3));
			memoryPanel.setBackground(Color.WHITE);
			memoryPanel.add(new JLabel("Location"));
			memoryPanel.add(new JLabel("Hex"));
			memoryPanel.add(new JLabel("Value"));
			
		JScrollPane memoryScroller = new JScrollPane(memoryPanel);	
		memoryScroller.setPreferredSize(new Dimension(300,100));
		memoryScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		rootPanel.add(memoryScroller);
				
		rootPanel.add(new JSeparator());
		
		JPanel nextInstructionPanel = new JPanel(new FlowLayout());
			nextInstruction = new JLabel();
			setNextInstruction(p.source[0]);
			nextInstructionPanel.add(nextInstruction);
		rootPanel.add(nextInstructionPanel);
		
		JPanel buttonPanel = new JPanel();
			buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
			buttonPanel.add(Box.createHorizontalGlue());
				this.step = new JButton("Step");
				this.step.addActionListener(this);
				buttonPanel.add(step);
			buttonPanel.add(Box.createHorizontalStrut(5));
				
				this.runAll = new JButton("Run All");
				this.runAll.addActionListener(this);
				buttonPanel.add(runAll);
			buttonPanel.add(Box.createHorizontalStrut(5));
		rootPanel.add(buttonPanel);
		
		rootPanel.add(Box.createRigidArea(new Dimension(300,5)));
		rootPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.add(rootPanel);
	}
	
	
	public void close() {
		this.setVisible(false);
		this.dispose();
	}

	private boolean executeNext() {
		State s = p.executeNext();
		for(int i=0;i<s.GPRs.length; i++) {
			final int j = i;//Required to allow j to be used in the lambda expression
			SwingUtilities.invokeLater(() -> {setGPRValue(j,s.GPRs[j]);});
			//invokeLater as Swing is not thread-safe
		}
		for(int i=0; i<s.memory.length; i++) {
			if(s.memory.get(i) > 0) {
				final int j = i;
				final long val = s.memory.get(i);
				SwingUtilities.invokeLater(() -> {this.setMemoryValue(j,val);});
			}
		}
		setNextInstruction(s.instruction);
		SwingUtilities.invokeLater(()-> {setLastComparisonValue(s.lastComparison);});
		if(!s.running) {
			SwingUtilities.invokeLater(() -> {
				runAll.setEnabled(false);
				step.setEnabled(false);
				this.setTitle(this.getTitle() + " [FINISHED]");
			});
		}
		return s.running;
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(arg0.getSource() == this.step) {
			this.executeNext();
		}
		else if(arg0.getSource() == this.runAll) {
			this.runAll.setEnabled(false);
			this.step.setEnabled(false);
			this.runAll.setText("Running...");
			Instant before = Instant.now();
			Duration timeoutDuration = Duration.ofSeconds(5);
			
			//Waits for (1) thread to finish before releasing await();
			CountDownLatch finishFirstLatch = new CountDownLatch(1);
			
			this.running = true;
			
			/* In this section, we set up a RACE CONDITION
			 * One of two things must happen:
			 * 
			 * 	1) The succesful execution of the program to a halted state (Reaching HALT or final line)
			 *  2) [5] seconds to elapse
			 *  
			 * Each of these tasks take up a single thread. Once a task has completed, its thread terminates
			 * A third thread will wait (block) until ONE of these conditions are met, and then cause the
			 * remaining thread to terminate by setting running = true, causing the second thread to return.
			 * 
			 */
			
			//1)
			Thread execThread = new Thread(() -> {
				
				/* executeNext() both calls the method and returns the status of the machine (is there another
				 * command to execute?) */
				while(this.executeNext()) {
					//if the other thread has terminated, it will set running = false
					if(!running) {
						return;
					}
				}
				//notify the latch that the condition has been met
				finishFirstLatch.countDown();
				runAll.setText("Finished");
			});
			execThread.start();
			
			Thread timeoutThread = new Thread(() -> {
				while(Duration.between(before, Instant.now()).minus(timeoutDuration).isNegative()) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						//Eh.
					}
					if(!running) {
						return;
					}
				}
				
				//JOptionPane will wait (block) until it is dismissed, we'll make a new thread so it doesn't block this thread.
				new Thread(() -> {
					JOptionPane.showMessageDialog(this, "Execution timed out (took more than " + timeoutDuration.toMillis() + "ms). Check your code for infinite loops.",
														"Execution halted",
														JOptionPane.ERROR_MESSAGE);
				}).start();
				
				//notify the latch that the condition has been met
				finishFirstLatch.countDown();
				runAll.setText("Timed out");
			});
			timeoutThread.start();
			
			//Finally create a new thread to wait for await(), so that the event thread handling the runAll click
			//can return. We must not block the Swing thread.
			new Thread(() -> {
				try {
					finishFirstLatch.await();
				} catch (InterruptedException e) {
					//Again, eh.
				}
				//stop the other thread
				this.running = false;
			}).start();
		}
		
	}
	
	
	
	
}
