/* StyleSwitcher.java
 * Streamlines switching the active DocumentSyntaxFormatter of a document
 * 
 */
package exmachina.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.text.StyledDocument;

public class StyleSwitcher implements ActionListener {

	Map<String, DocumentSyntaxFormatter> styles = new HashMap<String, DocumentSyntaxFormatter>();
	StyledDocument document;
	DocumentSyntaxFormatter currentStyle;
	
	public StyleSwitcher(StyledDocument doc) {
		this.document = doc;
	}
	
	public void putStyle(String key, DocumentSyntaxFormatter val) {
		styles.put(key, val);
	}
	
	public String[] getKeys() {
		return styles.keySet().toArray(new String[] {});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String key = (String) ((JComboBox<String>) arg0.getSource()).getSelectedItem();
		this.setStyle(key);
	}
	
	public boolean setStyle(String key) {
		if(styles.containsKey(key)) {
//			System.out.println("Switching to style " + key);
			this.document.removeDocumentListener(currentStyle);
			this.document.addDocumentListener(styles.get(key));
			this.currentStyle = styles.get(key);
			this.currentStyle.styleDocument(this.document);
			return true;
		}
		else {
			return false;
		}
	} 
	
}
