package exmachina.ui;

import java.awt.*;
import java.awt.event.ActionEvent;

import javax.swing.*;

public class RootFrame extends JFrame {
	
	JTextPane mainEditor;
	JComboBox<String> styleCombo;
	StyleSwitcher styleSwitcher;
	
	ExecuteFrame ef;
	
	public RootFrame() {
		initUI();
		setupStyles();
		mainEditor.setText("MOV R0, #26\nMOV R1, #7\nMOV R2, #0\nLOOP:\n\nCMP R1, #0\n\nBEQ END\nAND R3, R1, #1\nCMP R3, #0\nBEQ INC\nADD R2, R2, R0\nINC:\nLSL R0, R0, #1\nLSR R1, R1, #1\nB LOOP\nEND:\nHALT");
	}
	
	
	//Set up the main frame (window)
	private void initUI() {
		JPanel rootPanel = new JPanel();
		rootPanel.setLayout(new BoxLayout(rootPanel, BoxLayout.LINE_AXIS));
		
		//Set up the container panel for the editor
		JPanel editorPanel = new JPanel();
			editorPanel.setLayout(new BoxLayout(editorPanel, BoxLayout.PAGE_AXIS));
				this.mainEditor = new JTextPane();
			JScrollPane editorScroller = new JScrollPane(this.mainEditor);
			editorScroller.setPreferredSize(new Dimension(512,512));
			editorScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			editorScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			editorPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			editorPanel.add(editorScroller);
			
		rootPanel.add(editorPanel);
		
		//Set up the container panel for the sidepanel
		JPanel sidePanel = new JPanel();
			sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.PAGE_AXIS));//Layout each sub-pane below the previous one
			sidePanel.add(Box.createHorizontalStrut(300));//Keep the sidepanel at leat 300px wide
			
			JPanel docsPanel = new JPanel();
				docsPanel.setBackground(Color.WHITE);
				docsPanel.setLayout(new BoxLayout(docsPanel, BoxLayout.PAGE_AXIS));//Layout each documentation item below the previous one
					for(DocItem di : DocUtils.COMMANDS) {
						docsPanel.add(new DocComponent(di));
					}
			JScrollPane docsScroller = new JScrollPane(docsPanel);
			docsScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			docsScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
			docsScroller.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
			docsScroller.setPreferredSize(new Dimension(300,512));
			sidePanel.add(docsScroller);	
			
			JPanel buttonPanel = new JPanel();
//				buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
				buttonPanel.setLayout(new FlowLayout());//Temporary fix as JComboBox exhibits strange sizing behaviour under BoxLayout
				JButton executeButton = new JButton("Execute");
					
					executeButton.addActionListener((ActionEvent a) -> {
						if(ef != null) {
							ef.close();
						}
						
						ef = new ExecuteFrame(mainEditor.getText());
						
					});
				buttonPanel.add(executeButton);
				
				styleCombo = new JComboBox<String>();
				styleCombo.setPreferredSize(new Dimension(128,32));
				this.styleSwitcher = new StyleSwitcher(mainEditor.getStyledDocument());
				styleCombo.addActionListener(styleSwitcher);
				buttonPanel.add(styleCombo);
				
				buttonPanel.setPreferredSize(new Dimension(300,128));
			sidePanel.add(buttonPanel);
		rootPanel.add(sidePanel);
		
		this.add(rootPanel);
		this.pack();
		this.setTitle("ExMachina");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);

	}

	//add the styles to the styleSwitcher, and then to the comboBox
	private void setupStyles() {
		styleSwitcher.putStyle("Colourful", SyntaxStyles.SYNTAX_COLOURFUL);
		styleSwitcher.putStyle("Black & White", SyntaxStyles.SYNTAX_BLACKWHITE);
		
		for(String s : styleSwitcher.getKeys()) {
			this.styleCombo.addItem(s);
		}
		
		styleSwitcher.setStyle("Colourful");
	}
	
}
