/* DocumentSyntaxFormatter.java
 * Attaches to a Document (body of styled text) and listens for changes.
 * When changes are made, it unstyles and then restyles the document according
 * to the rules associated with it.
 */
package exmachina.ui;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

public class DocumentSyntaxFormatter implements DocumentListener {

	private final HashMap<Pattern,SimpleAttributeSet[]> complexStyles = new HashMap<Pattern,SimpleAttributeSet[]>();
	private SimpleAttributeSet defaultStyle = new SimpleAttributeSet();
	
	public DocumentSyntaxFormatter(StyledDocument doc) {
		doc.addDocumentListener(this);
		this.styleDocument(doc);
	}
	
	public DocumentSyntaxFormatter() {
		
	}
	
	public void setDefaultStyle(SimpleAttributeSet defaultStyle) {
		this.defaultStyle = defaultStyle;
	} 
	
	public void associate(Pattern pat, SimpleAttributeSet style) {
		complexStyles.put(pat, new SimpleAttributeSet[] {style});
	}

	public void associateAll(Pattern pat, SimpleAttributeSet... groupStyles) {
		complexStyles.put(pat, groupStyles);
	}
	
	public void styleDocument(StyledDocument doc) {
		SwingUtilities.invokeLater(() -> {doc.setCharacterAttributes(0, doc.getLength(), defaultStyle, true);});
		for(Entry<Pattern, SimpleAttributeSet[]> e: complexStyles.entrySet()) {
			try {
				final Matcher m = e.getKey().matcher(doc.getText(0, doc.getLength()));
				while(m.find()) {
					SimpleAttributeSet[] groupStyles = e.getValue();
					for(int i=0; i<groupStyles.length; i++) {
						if(groupStyles[i] != null) {
							int start = m.start(i), length = m.end(i) - m.start(i);
							SimpleAttributeSet style = groupStyles[i];
							SwingUtilities.invokeLater(()->{doc.setCharacterAttributes(start, length, style, false);});
						}
					}
				}
			} catch (BadLocationException e1) {e1.printStackTrace();}
		}
	}
	
	@Override
	public void insertUpdate(DocumentEvent arg0) {
		this.styleDocument((StyledDocument) arg0.getDocument());
	}

	@Override
	public void removeUpdate(DocumentEvent arg0) {
		this.styleDocument((StyledDocument) arg0.getDocument());
	}

	
	@Override
	public void changedUpdate(DocumentEvent arg0) {}//Not called outside of XML
	
}
