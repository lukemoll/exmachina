/* SyntaxStyles.java
 * Associates the attributes of the Colourful and Black&White themes
 * to the patterns of the different elements
 * 
 *  All code is run in a static initialiser block, and so SYNTAX_COLOURFUL
 *  and SYNTAX_BLACKWHITE may be referred to as constants
 */
package exmachina.ui;

import java.awt.Color;
import java.util.regex.Pattern;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

public class SyntaxStyles {

//	public final SimpleAttributeSet STYLE_CODE, STYLE_BRANCHING_NAME, STYLE_BRANCHING_PARAMS,  STYLE_COMPARISON_NAME, STYLE_COMPARISON_PARAMS,  STYLE_ARITHMETIC_NAME, STYLE_ARITHMETIC_PARAMS,  STYLE_LOAD_NAME, STYLE_LOAD_PARAMS,  STYLE_MOVE_NAME, STYLE_MOVE_PARAMS,  STYLE_BITWISE_NAME, STYLE_BITWISE_PARAMS,  STYLE_SHIFT_NAME, STYLE_SHIFT_PARAMS;
	
	public static final DocumentSyntaxFormatter SYNTAX_COLOURFUL, SYNTAX_BLACKWHITE;
	
	private static final SimpleAttributeSet COMMON_CODE, COMMON_COMMAND_NAME, COMMON_COMMAND_PARAMS, COMMON_COMMENT;
	
	static {
		COMMON_CODE = new SimpleAttributeSet();
		StyleConstants.setFontFamily(COMMON_CODE, "monospaced");
		StyleConstants.setFontSize(COMMON_CODE, 18);
		
		COMMON_COMMAND_NAME = (SimpleAttributeSet) COMMON_CODE.clone();
		StyleConstants.setBold(COMMON_COMMAND_NAME, true);
		COMMON_COMMAND_PARAMS = (SimpleAttributeSet) COMMON_CODE.clone();
		StyleConstants.setItalic(COMMON_COMMAND_PARAMS, true);
		
		COMMON_COMMENT = new SimpleAttributeSet();
		StyleConstants.setForeground(COMMON_COMMENT, Color.DARK_GRAY);
		StyleConstants.setItalic(COMMON_COMMENT, true);
	}
	
	static {//Initialise SYNTAX_COLOURFUL
	SimpleAttributeSet branchingNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(branchingNameStyle, Color.getHSBColor(0.108f, 1.0f, 1.0f));
	SimpleAttributeSet branchingParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(branchingParamsStyle, Color.getHSBColor(0.108f, 1.0f, 1.0f));

	SimpleAttributeSet comparisonNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(comparisonNameStyle, Color.getHSBColor(0.147f, 1.0f, 1.0f));
	SimpleAttributeSet comparisonParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(comparisonParamsStyle, Color.getHSBColor(0.147f, 1.0f, 1.0f));
	
	SimpleAttributeSet arithmeticNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(arithmeticNameStyle, Color.getHSBColor(0.297f, 1.0f, 0.6f));
	SimpleAttributeSet arithmeticParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(arithmeticParamsStyle, Color.getHSBColor(0.297f, 1.0f, 0.6f));
	
	SimpleAttributeSet loadNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(loadNameStyle, Color.getHSBColor(0.458f, 1.0f, 1.0f));
	SimpleAttributeSet loadParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(loadParamsStyle, Color.getHSBColor(0.458f, 1.0f, 1.0f));
	
	SimpleAttributeSet moveNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(moveNameStyle, Color.getHSBColor(0.536f, 1.0f, 1.0f));
	SimpleAttributeSet moveParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(moveParamsStyle, Color.getHSBColor(0.536f, 1.0f, 1.0f));
	
	SimpleAttributeSet bitwiseNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(bitwiseNameStyle, Color.getHSBColor(0.786f, 1.0f, 1.0f));
	SimpleAttributeSet bitwiseParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(bitwiseParamsStyle, Color.getHSBColor(0.786f, 1.0f, 1.0f));
	
	SimpleAttributeSet shiftNameStyle = (SimpleAttributeSet) COMMON_COMMAND_NAME.clone();
		StyleConstants.setForeground(shiftNameStyle, Color.getHSBColor(0.839f, 1.0f, 1.0f));
	SimpleAttributeSet shiftParamsStyle = (SimpleAttributeSet) COMMON_COMMAND_PARAMS.clone();
		StyleConstants.setForeground(shiftParamsStyle, Color.getHSBColor(0.839f, 1.0f, 1.0f));
	
	SYNTAX_COLOURFUL = new DocumentSyntaxFormatter();
		SYNTAX_COLOURFUL.setDefaultStyle(COMMON_CODE);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(B(?:[A-Z]{2})?) ([A-Z]+)$", Pattern.MULTILINE), null, branchingNameStyle, branchingParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^([A-Z]+)(:)$", Pattern.MULTILINE), null, branchingParamsStyle, branchingNameStyle);
		
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(CMP) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, comparisonNameStyle, comparisonParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(ADD|SUB) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, arithmeticNameStyle, arithmeticParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(LDR|STR) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, loadNameStyle, loadParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(MOV) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, moveNameStyle, moveParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(AND|ORR|EOR|MVN) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, bitwiseNameStyle, bitwiseParamsStyle);
		SYNTAX_COLOURFUL.associateAll(Pattern.compile("^(LSL|LSR) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, shiftNameStyle, shiftParamsStyle);
		
		SYNTAX_COLOURFUL.associate(Pattern.compile("^(?:[#@]|\\/{2}).*$",Pattern.MULTILINE), COMMON_COMMENT);
	}
	
	static {//Initialise SYNTAX_BLACKWHITE
		SYNTAX_BLACKWHITE = new DocumentSyntaxFormatter();
		SYNTAX_BLACKWHITE.setDefaultStyle(COMMON_CODE);
		SYNTAX_BLACKWHITE.associate(Pattern.compile("^(?:[#@]|\\/{2}).*$",Pattern.MULTILINE), COMMON_COMMENT);
		SYNTAX_BLACKWHITE.associateAll(Pattern.compile("^([A-Z]{1,3}) ((?:\\w+, )+[#\\w]+)$", Pattern.MULTILINE), null, COMMON_COMMAND_NAME, COMMON_COMMAND_PARAMS);
		SYNTAX_BLACKWHITE.associateAll(Pattern.compile("^(B(?:[A-Z]{2})?) ([A-Z]+)$", Pattern.MULTILINE), null, COMMON_COMMAND_NAME, COMMON_COMMAND_PARAMS);
		SYNTAX_BLACKWHITE.associateAll(Pattern.compile("^([A-Z]+)(:)$", Pattern.MULTILINE), null, COMMON_COMMAND_PARAMS, COMMON_COMMAND_NAME);	
	}
}
