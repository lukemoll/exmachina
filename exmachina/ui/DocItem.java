/* DocItem.java
 * Essentially a fancy struct, acts as a tuple to hold documentation about a command
 * in one object 
 */
package exmachina.ui;

public class DocItem {
	public final String COMMAND, PARAMS, DESCRIPTION;
	DocItem(String com, String prms, String desc) {
		this.COMMAND = com;
		this.PARAMS = prms;
		this.DESCRIPTION = desc;
	}
}
