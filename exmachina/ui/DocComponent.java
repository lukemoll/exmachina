/* DocComponent.java
 * A displayable component that shows documentation about a command.
 */
package exmachina.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

import exmachina.ui.DocItem;

public class DocComponent extends JPanel {

	private static final long serialVersionUID = 1L;

	private final String COMMAND, PARAMS, DESCRIPTION;
	
	public DocComponent(String command, String params, String description) {
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		
		this.COMMAND = command;
		this.PARAMS = params;
		this.DESCRIPTION = description;
		
		JLabel commandLabel = new JLabel(COMMAND);
		JLabel paramLabel = new JLabel(PARAMS);
		JLabel descLabel = new JLabel("<html><body style='width: 200px;'>" + DESCRIPTION + "</body></html>");
		Font baseFont = commandLabel.getFont(), commandFont, paramFont, descFont;
		
		commandFont = baseFont.deriveFont(Font.BOLD);
		paramFont = baseFont.deriveFont(Font.ITALIC);
		descFont = baseFont.deriveFont(Font.PLAIN);
		
		commandLabel.setFont(commandFont);
		paramLabel.setFont(paramFont);
		descLabel.setFont(descFont);
		descLabel.setAlignmentX(LEFT_ALIGNMENT);
		
		this.add(new JToolBar.Separator());
		
		JPanel headerPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
			headerPanel.add(commandLabel);
			headerPanel.add(paramLabel);
			headerPanel.setBackground(Color.WHITE);
			headerPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));
		this.add(headerPanel);
		JPanel descPanel = new JPanel();
			descPanel.add(descLabel);
			descPanel.setBackground(Color.WHITE);
		this.add(descPanel);
		
		
	}
	
	public DocComponent(DocItem i) {
		this(i.COMMAND,i.PARAMS,i.DESCRIPTION);
	}
}
